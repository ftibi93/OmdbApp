package com.tiborfarago.omdbapp

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar

fun Fragment.addFragment(fragment: Fragment) {
    this.activity?.let { activity ->
        activity.supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(
                        R.anim.details_enter_fragment_anim,
                        R.anim.details_exit_fragment_anim,
                        R.anim.details_pop_enter_fragment_anim,
                        R.anim.details_pop_exit_fragment_anim)
                .hide(this)
                .add(R.id.frame_layout, fragment)
                .addToBackStack(null)
                .commit()
    }
}

fun Fragment.openImage(fragment: Fragment) {
    this.activity?.let {activity ->
        activity.supportFragmentManager
                .beginTransaction()
                .setCustomAnimations(
                        R.anim.poster_enter_fragment_anim,
                        R.anim.poster_exit_fragment_anim)
                .add(R.id.frame_layout, fragment)
                .addToBackStack(null)
                .commit()
    }
}

fun Fragment.addToolbar(
        id: Int,
        hasUpButton: Boolean,
        title: String? = resources.getText(this.resources.getIdentifier("app_name", "string", activity?.packageName)).toString()) {
    val toolbar = view?.findViewById<Toolbar>(id)
    setHasOptionsMenu(true)
    (activity as MainActivity).apply {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(hasUpButton)
        supportActionBar?.title = title
    }
}

inline fun <T: Fragment> T.withArgs(argsBuilder: Bundle.() -> Unit): T {
    arguments = Bundle().apply(argsBuilder)
    return this
}