package com.tiborfarago.omdbapp.main

import com.tiborfarago.omdbapp.realm.Movie
import io.realm.RealmResults

interface MainContract {
    interface View {
        fun changeDataset(data: RealmResults<Movie>?)
        fun showSnackbar(message: String)
        fun showLoading(isLoading: Boolean)
        fun showFooter(isShown: Boolean)
    }
    interface Presenter {
        fun editTextChange(title: String)
        fun footerClick()
        fun attachView(view: View)
        fun detachView()
        fun initData()
    }
}