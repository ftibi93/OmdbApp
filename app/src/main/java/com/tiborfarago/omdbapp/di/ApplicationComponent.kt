package com.tiborfarago.omdbapp.di

import com.tiborfarago.omdbapp.details.DetailsFragment
import com.tiborfarago.omdbapp.main.MainFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(RetrofitModule::class)])
interface ApplicationComponent {
    fun inject(mainFragment: MainFragment)
    fun inject(detailsFragment: DetailsFragment)
}