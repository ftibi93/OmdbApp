package com.tiborfarago.omdbapp

import retrofit2.Call

abstract class BasePresenter {
    fun <T> cancelRequest(request: Call<T>?) {
        if (request != null && request.isCanceled)
            request.cancel()
    }
}