package com.tiborfarago.omdbapp.details

import android.util.Log
import android.view.View
import com.tiborfarago.omdbapp.BasePresenter
import com.tiborfarago.omdbapp.R
import com.tiborfarago.omdbapp.realm.Movie
import com.tiborfarago.omdbapp.retrofit.MovieDetails
import com.tiborfarago.omdbapp.retrofit.Service
import io.realm.Realm
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject
import io.realm.kotlin.where

class DetailsPresenter @Inject constructor(private val service: Service) : DetailsContract.Presenter, BasePresenter() {
    private var shouldAnimShow = false
    private var isFullPlotToShow = false

    private var realm = Realm.getDefaultInstance()
    private var imdbID: String? = null
    private var view: DetailsContract.View? = null

    private var call: Call<MovieDetails>? = null
    private lateinit var movie: Movie

    override fun attachView(view: DetailsContract.View) {
        this.view = view
    }

    override fun getMovie(imdbID: String?) {
        this.imdbID = imdbID
        movie = realm.where<Movie>().equalTo("imdbID", imdbID).findFirst()!!
        if (movie.isThereDetails()) showData()

        view?.loadCover(movie.poster)
        view?.setTitle(movie.title)
        view?.changeSaveButton(movie.isSaved)

        view?.setProgressBarVisibility(View.VISIBLE)
        call = service.movieDetails(movie.imdbID, "short")
        call?.enqueue(object : Callback<MovieDetails> {
            override fun onFailure(call: Call<MovieDetails>?, t: Throwable?) {
                // todo handle errors
                showData()
            }

            override fun onResponse(call: Call<MovieDetails>, response: Response<MovieDetails>) {
                if (!realm.isClosed)
                    realm.executeTransactionAsync({ realm ->
                        run {
                            val body = response.body()
                            realm.where<Movie>().equalTo("imdbID", imdbID).findFirst()?.apply {
                                shortPlot = body?.plot!!
                                rated = body.rated
                                runtime = body.runtime
                                genre = body.genre
                                director = body.director
                                writer = body.writer
                                actors = body.actors
                                language = body.language
                                awards = body.awards

                                body.ratings.forEach {
                                    if (it.source == "Internet Movie Database") imdbRating = it.value
                                    if (it.source == "Rotten Tomatoes") rottenTomatoesRating = it.value
                                    if (it.source == "Metacritic") metacriticRating = it.value
                                }

                                // These are needed!
                                if (body.released != null && body.released != "N/A") setReleaseDate(body.released)
                                if (body.dVD != null && body.dVD != "N/A") setDvdDate(body.dVD)
                                if (body.boxOffice != null) boxOffice = body.boxOffice
                                if (body.production != null) production = body.production
                                if (body.website != null) website = body.website
                            }
                        }
                    }, {
                        showData()
                    }, {
                        Log.e("Realm ERROR", it.message)
                        showData()
                    })
            }

        })
    }

    override fun onPlotFabClick() {
        isFullPlotToShow = !isFullPlotToShow
        if (!isFullPlotToShow) {
            view?.setFABsrc(R.drawable.ic_keyboard_arrow_down_black_36px)
            view?.setPlot(movie.shortPlot)
            return
        }
        if (isFullPlotToShow && movie.longPlot != "" && movie.longPlot != "N/A") {
            view?.setPlot(movie.longPlot)
            view?.setFABsrc(R.drawable.ic_keyboard_arrow_up_black_36px)
        }

        view?.setProgressBarVisibility(View.VISIBLE)
        call = service.movieDetails(movie.imdbID, "full")
        call?.enqueue(object : Callback<MovieDetails> {
            override fun onFailure(call: Call<MovieDetails>?, t: Throwable?) {
                // todo handle errors
            }

            override fun onResponse(call: Call<MovieDetails>, response: Response<MovieDetails>) {
                realm.executeTransactionAsync({realm -> run {
                    val movie = realm.where<Movie>().equalTo("imdbID", this@DetailsPresenter.imdbID).findFirst()
                    response.body()?.plot?.run {
                        movie?.longPlot = this
                    }
                } }, {
                    view?.setProgressBarVisibility(View.GONE)
                    view?.setFABsrc(R.drawable.ic_keyboard_arrow_up_black_36px)
                    view?.setPlot(movie.longPlot)
                }, {
                    view?.setProgressBarVisibility(View.GONE)
                    Log.e("Realm ERROR", it.message)
                })
            }
        })
    }

    fun showData() {
        view?.apply {
            setPlot(movie.shortPlot)
            setIMDBRating(movie.imdbRating)
            setRottenRating(movie.rottenTomatoesRating)
            setMetaRating(movie.metacriticRating)
            setDetails(movie)
            setProgressBarVisibility(View.GONE)
        }

        showScrollViewIfAnimEnded()
    }

    override fun showScrollViewIfAnimEnded() {
        if (shouldAnimShow) view?.setScrollViewVisibility(View.VISIBLE)
        else shouldAnimShow = true
    }

    override fun onFullScreenClick() {
        view?.showPoster(movie.poster)
    }

    override fun detachView() {
        view = null
        realm.close()
        cancelRequest(call)
    }

    override fun onSaveButtonClick() {
        realm.executeTransaction {
            movie.isSaved = !movie.isSaved
        }
        view?.changeSaveButton(movie.isSaved)
    }

    override fun isMovieSaved(): Boolean {
        if (!realm.isClosed) return movie.isSaved
        return false
    }
}