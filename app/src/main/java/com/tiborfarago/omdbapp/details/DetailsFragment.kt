package com.tiborfarago.omdbapp.details

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.res.ResourcesCompat
import android.support.v4.graphics.ColorUtils
import android.support.v7.graphics.Palette
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.tiborfarago.omdbapp.*

import com.tiborfarago.omdbapp.realm.Movie
import kotlinx.android.synthetic.main.fragment_details.*
import javax.inject.Inject

class DetailsFragment : Fragment(), DetailsContract.View {
    private val ORIENTATION_CHANGE = "orientation_change"
    private var imdbID: String? = null

    private var menu: Menu? = null

    @Inject lateinit var detailsPresenter: DetailsPresenter

    override fun onStart() {
        super.onStart()

        addToolbar(R.id.toolbar_details_fragment, true, "Details")
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        (context?.applicationContext as App).applicationComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            imdbID = it.getString(IMDB_ID)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onDestroyView() {
        super.onDestroyView()

        detailsPresenter.detachView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState != null && savedInstanceState.getBoolean(ORIENTATION_CHANGE, false))
            detailsPresenter.showScrollViewIfAnimEnded()

        detailsPresenter.attachView(this)
        detailsPresenter.getMovie(imdbID)

        details_fragment_full_screen_button.setOnClickListener {
            detailsPresenter.onFullScreenClick()
        }

        details_fragment_plot_fab.setOnClickListener {
            detailsPresenter.onPlotFabClick()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putBoolean(ORIENTATION_CHANGE, true)
    }

    override fun setDetails(movie: Movie) {
        details_fragment_release_text_view.text = movie.getReleaseDate()
        details_fragment_runtime_text_view.text = movie.runtime
        details_fragment_genre_text_view.text = movie.genre
        details_fragment_actors_text_view.text = movie.actors
        details_fragment_writer_text_view.text = movie.writer
        details_fragment_director_text_view.text = movie.director
        details_fragment_awards_text_view.text = movie.awards
        details_fragment_dvd_text_view.text = movie.getDvdDate()
        details_fragment_production_text_view.text = movie.production
        details_fragment_boxoffice_text_view.text = movie.boxOffice
        details_fragment_website_text_view.text = movie.website
    }

    override fun setFABsrc(drawable: Int) {
        details_fragment_plot_fab.setImageResource(drawable)
    }

    override fun setProgressBarVisibility(visibility: Int) {
        details_fragment_progress_bar.visibility = visibility
    }

    override fun setScrollViewVisibility(visibility: Int) {
        details_fragment_nested_scroll_view.visibility = visibility
    }

    override fun setPlot(plot: String?) {
        details_fragment_plot_text_view.text = plot

        details_fragment_plot_card_view.layoutParams.apply {
            height = ViewGroup.LayoutParams.WRAP_CONTENT
        }
    }

    override fun setIMDBRating(rating: String) {
        details_fragment_imdb_textView.text = rating
    }

    override fun setRottenRating(rating: String) {
        details_fragment_rotten_textView.text = rating
    }

    override fun setMetaRating(rating: String) {
        details_fragment_meta_textView.text = rating
    }

    override fun setTitle(title: String) {
        details_fragment_title_text_view.text = title
    }

    override fun showPoster(url: String) {
        openImage(PosterFragment.newInstance(url))
    }

    override fun loadCover(url: String) {
        // todo further optimization
        Glide.with(context!!)
                .load(url)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                        return false
                    }

                    override fun onResourceReady(resource: Drawable, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                        val bitmap = (resource as BitmapDrawable).bitmap
                        Palette.from(bitmap).generate {
                            details_fragment_title_card_view?.setCardBackgroundColor(it.getLightMutedColor(Color.WHITE))

                            val out = FloatArray(3)
                            ColorUtils.colorToHSL(it.getLightMutedColor(Color.WHITE), out)
                            out[2] = 0.9f

                            val backgroundColor = ColorUtils.HSLToColor(out)
                            details_fragment_nested_scroll_view?.setBackgroundColor(backgroundColor)
                        }

                        return false
                    }
                })
                .into(details_fragment_image_view)
    }

    companion object {
        private const val IMDB_ID = "param1"

        fun newInstance(imdbID: String?): DetailsFragment =
                DetailsFragment().withArgs {
                    putString(IMDB_ID, imdbID)
                }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> activity?.onBackPressed()
            R.id.menu_item_details_save -> detailsPresenter.onSaveButtonClick()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_details_save, menu)
        this.menu = menu
        super.onCreateOptionsMenu(menu, inflater)
        changeSaveButton(detailsPresenter.isMovieSaved())
    }

    override fun changeSaveButton(isSaved: Boolean) {
        menu?.findItem(R.id.menu_item_details_save)?.apply {
            icon = if (isSaved) ResourcesCompat.getDrawable(resources, R.drawable.ic_cloud_done_white_24px, null)
            else ResourcesCompat.getDrawable(resources, R.drawable.ic_cloud_download_white_24px, null)
        }
    }

    override fun onCreateAnimation(transit: Int, enter: Boolean, nextAnim: Int): Animation {
        val animator: Animation?
        if (enter && nextAnim == R.anim.details_enter_fragment_anim) {
            animator = AnimationUtils.loadAnimation(activity, nextAnim)
            if (animator != null && enter) {
                animator.setAnimationListener(object : Animation.AnimationListener {
                    override fun onAnimationRepeat(p0: Animation?) {}
                    override fun onAnimationStart(p0: Animation?) {}

                    override fun onAnimationEnd(p0: Animation?) {
                        detailsPresenter.showScrollViewIfAnimEnded()
                    }
                })
            }
        } else return AnimationUtils.loadAnimation(activity, R.anim.details_exit_fragment_anim)

        return animator!!
    }
}
