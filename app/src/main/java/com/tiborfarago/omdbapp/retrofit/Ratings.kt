package com.tiborfarago.omdbapp.retrofit

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName



data class Ratings(
        @SerializedName("Source")
        @Expose
        val source: String,
        @SerializedName("Value")
        @Expose
        val value: String
)