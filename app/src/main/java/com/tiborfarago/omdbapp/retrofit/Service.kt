package com.tiborfarago.omdbapp.retrofit

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface Service {
    @GET("/")
    fun movieList(@Query("s") title: String,
                  @Query("page") page: Int = 1,
                  @Query("apikey") apikey: String = "c1f68b29")
            : Call<MovieList>

    @GET("/")
    fun movieDetails(@Query("i") imdbID: String,
                     @Query("plot") plot: String,
                     @Query("apikey") apikey: String = "c1f68b29")
            : Call<MovieDetails>
}