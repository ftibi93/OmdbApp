package com.tiborfarago.omdbapp.androidjob

import com.evernote.android.job.Job
import com.evernote.android.job.JobManager
import com.evernote.android.job.JobRequest
import com.tiborfarago.omdbapp.realm.Movie
import io.realm.Realm
import io.realm.kotlin.deleteFromRealm

class MovieJob : Job() {
    override fun onRunJob(params: Params): Result {
        val realm = Realm.getDefaultInstance()

        try {
            realm.executeTransaction {
                it.where(Movie::class.java).equalTo("isSaved", false)
                        .run {
                            findAll()
                                    .subList(0, count().toInt().minus(LIMIT))
                                    .forEach { it.deleteFromRealm() } }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            realm.close()
        }
        return Result.SUCCESS
    }

    companion object {
        const val TAG = "MovieJob"
        const val LIMIT = 100
        private var jobId = -1

        fun scheduleJob() {
            jobId = JobRequest.Builder(TAG)
                    .setExact(1000 * 60 * 60 * 6)
                    .build()
                    .schedule()
        }

        fun cancelJob() {
            JobManager.instance().cancel(jobId)
        }
    }
}