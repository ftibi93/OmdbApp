package com.tiborfarago.omdbapp.androidjob

import com.evernote.android.job.Job
import com.evernote.android.job.JobCreator

class JobCreator : JobCreator {
    override fun create(tag: String): Job? {
        return when (tag) {
            MovieJob.TAG -> MovieJob()
            else -> null
        }
    }
}