package com.tiborfarago.omdbapp

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.tiborfarago.omdbapp.androidjob.MovieJob
import com.tiborfarago.omdbapp.main.MainContract
import com.tiborfarago.omdbapp.main.MainFragment
import kotlinx.android.synthetic.main.toolbar.*

class MainActivity : AppCompatActivity() {
    private lateinit var fragmentManager: FragmentManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        fragmentManager = supportFragmentManager

        if (fragmentManager.findFragmentByTag(MainFragment::class.java.name) == null) {
            fragmentManager
                    .beginTransaction()
                    .add(R.id.frame_layout, MainFragment.newInstance(), MainFragment::class.java.name)
                    .commit()
        }
    }

    override fun onPause() {
        super.onPause()
        MovieJob.scheduleJob()
    }

    override fun onResume() {
        super.onResume()
        MovieJob.cancelJob()
    }

    override fun onBackPressed() {
        val fragment: Fragment? = fragmentManager.findFragmentByTag(MainFragment::class.java.name)
        if (!(fragment as MainFragment).onBackPressed())
            super.onBackPressed()
    }
}
